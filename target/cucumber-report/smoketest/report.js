$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Test3.feature");
formatter.feature({
  "name": "тестовое задание №3",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "Зайти на \"google.com\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.зайтиНа(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Выполнить поиск по запросу \"Альфа банк\"",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.выполнитьПоискПоЗапросу(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Перейти на старницу Альфа банк",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.перейтиНаСтарницуАльфаБанк()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Перейти в раздел Вакансии",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.перейтиВРазделВакансии()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Перейти в раздел О работе в банке",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.перейтиВРазделОРаботеВБанке()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Взять текст со страницы и сохранить его в файл",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.взятьТекстСоСтраницыИСохранитьЕгоВФайл()"
});
formatter.result({
  "status": "passed"
});
});