import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverProvider;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.commands.GetName;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebDriverBuilder;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.*;

public class MyStepdefs {

    String productName;

    @Given("^Зайти на \"([^\"]*)\"$")
    public void зайтиНа(String link) {
        Selenide.open("http://" + link);
    }

    @When("^Выбрать производителя \"([^\"]*)\"$")
    public void выбратьПроизводителя(String name) {
        if (!$(By.xpath("//span[text()='" + name + "']")).scrollIntoView(false).isSelected())
            $(By.xpath("//span[text()='" + name + "']")).click();
    }

    @When("^Выбрать \"([^\"]*)\"-ый продукт из списка и записать его название$")
    public void выбратьПродукт(String productNumber) {
        productName = $$(By.xpath("//div[@class='n-snippet-cell2__title']//a")).get(Integer.parseInt(productNumber) - 1).getText();
    }

    @And("^Установить фильтр по цене от \"([^\"]*)\" и до \"([^\"]*)\"$")
    public void установитьФильтрПоЦенеОтИДо(String priceFrom, String priceTo) {
        $(By.id("glpricefrom")).setValue(priceFrom);
        if (priceTo != null) {
            $(By.id("glpriceto")).setValue(priceTo);
        }
        $(withText("Samsung")).waitUntil(visible, 5000);
    }

    @And("^Зайти в карточку \"([^\"]*)\"-го продукта в списке$")
    public void зайтиВКарточкуПродукта(String productNumber) {
        $$(By.xpath("//div[@class='n-snippet-cell2__title']//a")).get(Integer.parseInt(productNumber) - 1).click();
    }

    @When("^Перейти в \"([^\"]*)\"$")
    public void перейтиВ(String service) {
        $$(By.cssSelector("[data-id]")).find(text(service)).click();
    }

    @Given("^Отсортировать значения из файла \"([^\"]*)\" по возрастанию и вывести на экран$")
    public void отсортироватьЗначенияИзФайлаПоВозрастаниюИВывестиНаЭкран(String fileD) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileD));
        StringBuilder sb = new StringBuilder();
        while (reader.ready()) {
            sb.append(reader.readLine().replaceAll("\\s", ""));
        }
        String[] strings = sb.toString().split(",");
        Integer[] myArr = new Integer[strings.length];
        for (int i = 0; i < strings.length; i++) {
            myArr[i] = Integer.parseInt(strings[i]);
        }
        Arrays.sort(myArr);
        System.out.println("Отсортированные значения по возрастанию");
        for (int arr : myArr) {
            System.out.println(arr);
        }
    }

    @Given("^Отсортировать значения из файла \"([^\"]*)\" по убыванию и вывести на экран$")
    public void отсортироватьЗначенияИзФайлаПоУбываниюИВывестиНаЭкран(String fileD) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileD));
        StringBuilder sb = new StringBuilder();
        while (reader.ready()) {
            sb.append(reader.readLine().replaceAll("\\s", ""));
        }
        String[] strings = sb.toString().split(",");
        Integer[] myArr = new Integer[strings.length];
        for (int i = 0; i < strings.length; i++) {
            myArr[i] = Integer.parseInt(strings[i]);
        }
        Arrays.sort(myArr, Collections.reverseOrder());
        System.out.println("Отсортированные значения по убыванию");
        for (int arr : myArr) {
            System.out.println(arr);
        }
    }

    @When("^Перейти в раздел \"([^\"]*)\", в подраздел \"([^\"]*)\"$")
    public void перейтиВРазделВПодраздел(String section1, String section2) {
        $$(By.xpath("//li//a")).find(text(section1)).click();
        $$(By.xpath("//div//a")).find(text(section2)).click();
    }

    @And("^выполить ожидание в течении (\\d+) (?:секунд|секунды)$")
    public void выполнитьОжидание(long seconds) {
        sleep(1000 * seconds);
    }

    @Then("^Сравнить название телефона из заголовка с ранее записаным названием$")
    public void сравнитьНазваниеТелефонаИзЗаголовкаСРанееЗаписанымНазванием() {
        Assert.assertEquals("Названия не совпадают", productName, $(By.xpath("//h1")).getText());
    }

    @When("^Выполнить поиск по запросу \"([^\"]*)\"$")
    public void выполнитьПоискПоЗапросу(String text) {
        $(By.name("q")).setValue(text).pressEnter();
    }

    @When("^Перейти на старницу Альфа банк$")
    public void перейтиНаСтарницуАльфаБанк() throws Throwable {
        $$(By.xpath("//h3//a")).find(attribute("href", "https://alfabank.ru/")).click();
    }

    @When("^Перейти в раздел Вакансии$")
    public void перейтиВРазделВакансии() {
        $(By.xpath("//a[text()='Вакансии']")).click();
    }

    @When("^Перейти в раздел О работе в банке$")
    public void перейтиВРазделОРаботеВБанке() {
        $(By.xpath("//a/span[text()='О работе в банке']")).click();
        sleep(4000);
    }

    @When("^Взять текст со страницы и сохранить его в файл$")
    public void взятьТекстСоСтраницыИСохранитьЕгоВФайл() throws IOException {
        String str = $(By.className("message")).getText();
        str = str + "\n" + $(By.className("info")).getText();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        Date date = new Date();

        WebDriver wd = WebDriverRunner.getWebDriver();
        Capabilities capabilities = ((RemoteWebDriver) wd).getCapabilities();
        String browserName = capabilities.getBrowserName();
        System.out.println(wd);

        String path = "TestProject\\" + browserName + "_" + dateFormat.format(date) + ".txt";
        File myFile = new File(path);

        FileWriter fw = new FileWriter(myFile);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(str);
        bw.close();
    }
}
